#!/usr/bin/env bash

set -eu -o pipefail

echo
echo == Deploying to Releasing targets
echo
scap deploy --environment releasing -f
