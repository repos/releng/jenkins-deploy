// Variables defined here are available to all .groovy job definitions in this
// directory.

// Environment variables to set up trainbranchbot as the git author/committer
Map trainBranchBotEnvs = [
  GIT_AUTHOR_NAME: 'trainbranchbot',
  GIT_AUTHOR_EMAIL: 'trainbranchbot@releases-jenkins.wikimedia.org',
  GIT_COMMITTER_NAME: 'trainbranchbot',
  GIT_COMMITTER_EMAIL: 'trainbranchbot@releases-jenkins.wikimedia.org',
]

// Environment variables to ensure timely output from Python scripts to the
// Jenkins console
Map pythonUnbufferedEnvs = [
  PYTHONUNBUFFERED: '1',
]
