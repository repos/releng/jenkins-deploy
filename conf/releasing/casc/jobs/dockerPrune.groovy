job('docker-system-prune') {
  description('''Weekly maintenance job that removes all stopped Docker containers, unused images, and build caches.
    <p><a href="https://gitlab.wikimedia.org/repos/releng/jenkins-deploy/-/blob/master/conf/releasing/casc/jobs/dockerPrune.groovy">
    Job definition</a>.</p>
  '''.stripIndent())

  triggers {
    cron('H 0 * * 0')
  }

  steps {
    shell('docker system prune -af')
  }
}