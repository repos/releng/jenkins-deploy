job('MediaWiki branch and publish WMF single-version image') {
  description('''\
  <p>
  This job cuts a long-lived WMF MediaWiki branch (defaults to
  <code>wmf/next</code>) and triggers the downstream job <i>MediaWiki publish
  WMF single-version image</i> which builds and publishes a WMF production
  image containing only that version.
  </p>

  <p><a href="https://gitlab.wikimedia.org/repos/releng/jenkins-deploy/-/blob/master/conf/releasing/casc/jobs/branchMWSingleVersion.groovy">
  Job definition</a>.</p>
  '''.stripIndent())

  parameters {
    stringParam {
      name('WMF_MW_VERSION')
      description('The WMF MediaWiki version to branch and include in the single-version production image. For example "1.43.0-wmf.22" or "next".')
      defaultValue('next')
      trim(true)
    }
  }

  concurrentBuild()

  scm {
    git {
      remote {
        url('https://gitlab.wikimedia.org/repos/releng/release.git')
      }
      branch('*/main')
    }
  }

  triggers {
    parameterizedCron {
        parameterizedSpecification('''\
          TZ=Europe/London
          # min hour day-of-month month dayofweek
          # daily branch cut at 0100 UTC
          0 1 * * *
          '''.stripIndent()
        )
    }
  }

  wrappers {
    credentialsBinding {
      // trainbranchbot.netrc
      file('NETRC', '2131fbf2-88f1-4fdc-949a-12f488ed79f1')
    }
    timestamps()
  }

  environmentVariables {
    envs(trainBranchBotEnvs)
    envs(pythonUnbufferedEnvs)
  }

  steps {
    // The CasC plugin uses the `${}` braces syntax to access variables. Using `^$` below to escape variables allows
    // them to make its way to the shell script without being interpolated by CasC
    shell('''\
      #!/bin/bash
      set -eu -o pipefail

      make-release/branch.py \
        --delete \
        --abandon \
        --skip-tag \
        --core \
        --bundle wmf_branch \
        wmf/$WMF_MW_VERSION

      make-release/branch.py \
        --core \
        --core-bundle wmf_core \
        --bundle wmf_branch \
        --branchpoint HEAD \
        --core-version keep \
        --push-option l=Code-Review+2 \
        wmf/$WMF_MW_VERSION
      '''.stripIndent()
    )
  }

  publishers {
    downstreamParameterized {
        trigger('MediaWiki publish WMF single-version image') {
            condition('SUCCESS')
            parameters {
                currentBuild()
            }
        }
    }

    extendedEmail {
      recipientList('releng@lists.wikimedia.org')
      triggers {
        failure {
          sendTo {
            recipientList()
          }
        }
        fixed {
          sendTo {
            recipientList()
          }
        }
      }
    }
  }
}
