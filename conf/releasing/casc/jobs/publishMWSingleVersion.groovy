job('MediaWiki publish WMF single-version image') {
  description('''\
  <p>
  This job builds and publishes a single version WMF production image for the
  given MediaWiki version (defaulting to <i>next</i>).
  </p>

  <p><a href="https://gitlab.wikimedia.org/repos/releng/jenkins-deploy/-/blob/master/conf/releasing/casc/jobs/publishMWSingleVersion.groovy">
  Job definition</a>.</p>
  '''.stripIndent())

  parameters {
    stringParam {
      name('WMF_MW_VERSION')
      description('The WMF MediaWiki version to include in the production image. For example "1.43.0-wmf.22" or "next".')
      defaultValue('next')
      trim(true)
    }
  }

  concurrentBuild()

  scm {
    git {
      remote {
        url('https://gitlab.wikimedia.org/repos/releng/release.git')
      }
      branch('*/main')
      extensions {
        relativeTargetDirectory('release')
      }
    }
  }

  wrappers {
    timestamps()
  }

  environmentVariables {
    envs(trainBranchBotEnvs)
    envs(pythonUnbufferedEnvs)
  }

  steps {
    // The CasC plugin uses the `${}` braces syntax to access variables. Using `^$` below to escape variables allows
    // them to make its way to the shell script without being interpolated by CasC
    shell('''\
      #!/bin/bash
      set -eu -o pipefail

      mw_local_repo="$(pwd)/repos/mediawiki/core.git"
      release="$(pwd)/release"
      patches="$(pwd)/patches"
      private="$(pwd)/private"
      mwstage="$(pwd)/mediawiki"
      scap_flags=(
        -Dstage_dir:"$mwstage"
        -Dpatch_path:"$patches"
        -Drelease_repo_dir:"$release"
        -Drelease_repo_update_cmd:
        -Drelease_repo_build_and_push_images_cmd:./build-images.py
        -Ddocker_user:"$USER"
        -Dtcpircbot_host:
      )

      cleanup() {
        if [ -d "$mwstage" ]; then
          scap mwshell --directory "$mwstage" --user www-data -- \
            "find '$mwstage' -type f -user www-data -delete"
          scap mwshell --directory "$mwstage" --user www-data -- \
            "find '$mwstage' -type d -user www-data -delete"
          rm -rf "$mwstage"
        fi
      }

      trap cleanup TERM EXIT
      cleanup

      if ! [ -d "$mw_local_repo" ]; then
        echo Creating local mediawiki/core git reference
        mkdir -p "$mw_local_repo"
        git init --bare "$mw_local_repo"
        git -C "$mw_local_repo" remote add origin https://gerrit.wikimedia.org/r/mediawiki/core
      fi

      echo Updating local mediawiki/core git reference
      git -C "$mw_local_repo" fetch --all --prune

      echo Copying patches from the production deployment server
      rsync -a --delete rsync://deployment.eqiad.wmnet/srv-patches-releases-primary/ "$patches"/
      echo

      echo Copying private settings from the production deployment server
      rsync -a --delete rsync://deployment.eqiad.wmnet/srv-mediawiki-private-primary/ "$private"/
      echo

      scap prep --auto \
        "^${scap_flags[@]}" \
        --copy-private-settings "^${private}/*.php" \
        --reference "$mw_local_repo" \
        "$WMF_MW_VERSION"

      scap build-images \
        "^${scap_flags[@]}" \
        --single-version "$WMF_MW_VERSION" \
        --latest-tag "$WMF_MW_VERSION" \
        "publishing wmf/$WMF_MW_VERSION image"
      '''.stripIndent()
    )
  }

  publishers {
    extendedEmail {
      recipientList('releng@lists.wikimedia.org')
      triggers {
        failure {
          sendTo {
            recipientList()
          }
        }
        fixed {
          sendTo {
            recipientList()
          }
        }
      }
    }
  }
}
