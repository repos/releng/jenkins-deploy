job('Branch cut test patches') {
  description('''This job tests security patches on a MediaWiki branch. It can use the branch for the current week's
    MediaWiki train or the <i>wmf/branch_cut_pretest</i> branch
    <p><a href="https://gitlab.wikimedia.org/repos/releng/jenkins-deploy/-/blob/master/conf/releasing/casc/jobs/branchCutPatches.groovy">
    Job definition</a>.</p>
  '''.stripIndent())
  parameters {
    stringParam {
      name('TEST_COMMIT')
      description('The name of the branch of releng/release to check out.  If not supplied, the default branch is used.')
      trim(true)
    }
    choiceParam(
        'MODE',
        ['pretest', 'train_branch'],
        '''\
         <ul>
          <li>pretest: Run a pretest using <i>wmf/branch_cut_pretest</i> (Default)</li>
          <li>train_branch: Use the MediaWiki branch for current week's train release</li>
        </ul>
        '''.stripIndent()
    )
  }

  concurrentBuild()

  scm {
    git {
      remote {
        url('https://gitlab.wikimedia.org/repos/releng/release.git')
      }
      branch('*/main')
    }
  }

  wrappers {
    timestamps()
    credentialsBinding {
      string('PATCH_BOT_TOKEN', 'security-patch-bot-conduit-token')
    }
  }

  environmentVariables {
    envs(trainBranchBotEnvs)
    envs(pythonUnbufferedEnvs)
  }

  steps {
    // The CasC plugin uses the `${}` braces syntax to access variables. Using `^$` below to escape variables allows
    // them to make its way to the shell script without being interpolated by CasC
    shell('''\
      #!/bin/bash
      set -eu -o pipefail

      if [ "^${TEST_COMMIT:-}" ]; then
        echo
        git fetch origin "$TEST_COMMIT"
        git checkout FETCH_HEAD
        git log -1
        echo
      fi
      case "$MODE" in
        pretest)
            TEST_PATCHES_BRANCH=branch_cut_pretest ;;
        train_branch)
            TEST_PATCHES_BRANCH=$(curl -s https://train-blockers.toolforge.org/api.php | jq -r '.current.version') ;;
        *)
            TEST_PATCHES_BRANCH=branch_cut_pretest ;;
      esac

      make-release/branch-cut-test-patches $TEST_PATCHES_BRANCH
      '''.stripIndent()
    )
  }

  publishers {
    extendedEmail {
      recipientList('releng@lists.wikimedia.org')
      triggers {
        failure {
          sendTo {
            recipientList()
          }
        }
        fixed {
          sendTo {
            recipientList()
          }
        }
      }
    }
  }
}
