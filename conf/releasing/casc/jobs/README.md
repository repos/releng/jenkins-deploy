Jobs in this dir are defined using the [Jenkins Job DSL](https://github.com/jenkinsci/job-dsl-plugin/wiki).

All Groovy files in this directory are concatenated together to define a single `script` entry in the [JCasC
format](https://github.com/jenkinsci/job-dsl-plugin/blob/master/docs/JCasC.md) during deployment (see
`scap/scripts/generate_casc_jobs.sh`).

When writing/modifying the definition of a job, you'll need to know what API methods are available on the Jenkins
instance. Every deployed Jenkins that has the Job DSL plugin installed offers an API viewer at
http(s)://jenkins_host/plugin/job-dsl/api-viewer/index.html. The methods available will vary depending on the instance,
as they are generated dynamically based on the plugins installed.

To test changes, you can create a temporary free-style job on Jenkins. Choose "Process Job DSLs" as the build step and
then put your DSL script in the "Use the provided DSL script" field. Then run the job; this will create a second job as
defined by your DSL script (you may have to approve the script in the "Manage Jenkins" section). Once you're finished
with your testing, delete both jobs.

As a playground for your changes, you can use the [Scap 3 development environment](https://gitlab.wikimedia.org/repos/releng/scap3-dev).
