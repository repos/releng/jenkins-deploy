job('Automatic branch cut') {
  description('''This job cuts a MediaWiki branch. It can create the weekly branch for the MediaWiki train or run a
    pretest using the <i>wmf/branch_cut_pretest</i> branch.
    <p><a href="https://gitlab.wikimedia.org/repos/releng/jenkins-deploy/-/blob/master/conf/releasing/casc/jobs/branchCut.groovy">
    Job definition</a>.</p>
  '''.stripIndent())
  parameters {
    stringParam {
      name('TEST_COMMIT')
      description('A reference to the commit of https://gitlab.wikimedia.org/repos/releng/release to use')
      trim(true)
    }
    choiceParam(
        'MODE',
        ['pretest', 'train_branch'],
        '''\
         <ul>
          <li>pretest: Run a pretest using <i>wmf/branch_cut_pretest</i> (Default)</li>
          <li>train_branch: Create new MediaWiki branch for current week's train release</li>
        </ul>
        '''.stripIndent()
    )
  }

  concurrentBuild()

  scm {
    git {
      remote {
        url('https://gitlab.wikimedia.org/repos/releng/release.git')
      }
      branch('*/main')
    }
  }

  triggers {
    parameterizedCron {
        parameterizedSpecification('''\
          TZ=Europe/London
          # min hour day-of-month month dayofweek
          # nightly pretest of wmf/branch_cut_pretest branch everyday at 0030 UTC
          30 0 * * * %MODE=pretest
          # weekly branch cut on Tuesdays at 0200 UTC
          0 2 * * 2 %MODE=train_branch
        '''.stripIndent()
        )
    }
  }

  wrappers {
    credentialsBinding {
      // trainbranchbot.netrc
      file('NETRC', '2131fbf2-88f1-4fdc-949a-12f488ed79f1')
    }
    timestamps()
  }

  environmentVariables {
    envs(trainBranchBotEnvs)
    envs(pythonUnbufferedEnvs)
  }

  steps {
    // The CasC plugin uses the `${}` braces syntax to access variables. Using `^$` below to escape variables allows
    // them to make its way to the shell script without being interpolated by CasC
    shell('''\
      #!/bin/bash
      set -eu -o pipefail

      if [ "^${TEST_COMMIT:-}" ]; then
        echo
        git fetch origin "$TEST_COMMIT"
        git checkout FETCH_HEAD
        git log -1
        echo
      fi
      case "$MODE" in
        pretest)
            make-release/automatic-branch-cut --test ;;
        train_branch)
            make-release/automatic-branch-cut ;;
        *)
            make-release/automatic-branch-cut --test ;;
      esac
      '''.stripIndent()
    )
  }

  publishers {
    downstreamParameterized {
        trigger('Branch cut test patches') {
            condition('SUCCESS')
            parameters {
                currentBuild()
            }
        }
    }

    extendedEmail {
      recipientList('releng@lists.wikimedia.org')
      defaultSubject('$PROJECT_NAME ($MODE) - Build # $BUILD_NUMBER - $BUILD_STATUS!')
      defaultContent('''$PROJECT_NAME ($MODE) - Build # $BUILD_NUMBER - $BUILD_STATUS:

Check console output at $BUILD_URL to view the results.            

If you get a "Not Found" page, log in at https://releases-jenkins.wikimedia.org/ first.
''')
      triggers {
        failure {
          sendTo {
            recipientList()
          }
        }
        fixed {
          sendTo {
            recipientList()
          }
        }
      }
    }
  }
}
