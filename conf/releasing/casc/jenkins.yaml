credentials:
  system:
    domainCredentials:
      - credentials:
        - usernamePassword:
            id: "ReleaseNotesBot"
            description: "Bot for posting release notes to MW.org"
            username: "ReleaseNotesBot@jenkins"
            password: "${release_notes_bot_pass}"
            usernameSecret: true
            scope: GLOBAL
        - usernamePassword:
            id: "integration-docker-registry.wmflabs.org"
            description: "Pipeline image publisher for integration-docker-registry.wmflabs.org"
            username: "integration-registry"
            password: "${integration_registry_pass}"
            usernameSecret: true
            scope: GLOBAL
        - usernamePassword:
            id: "doc_rsync_creds"
            description: "Rsync credentials to publish to doc hosts"
            username: "doc-publisher"
            password: "${doc_rsync_pass}"
            usernameSecret: true
            scope: GLOBAL
        - basicSSHUserPrivateKey:
            id: "releases_jenkins_rsa"
            username: "jenkins-agent"
            passphrase: "${releases_jenkins_rsa_pass}"
            privateKeySource:
              directEntry:
                privateKey: "${releases_jenkins_rsa_key}"
            usernameSecret: true
            scope: GLOBAL
        - file:
            id: "2131fbf2-88f1-4fdc-949a-12f488ed79f1"
            description: "Gerrit http credentials for train branch cut"
            fileName: "trainbranchbot.netrc"
            secretBytes: "${base64:${trainbranchbot_netrc}}"
            scope: GLOBAL
        - string:
            id: "jenkins-phab-conduit-token"
            description: "Generated to find the train task ID"
            secret: "${jenkins_phab_conduit_token}"
            scope: GLOBAL
        - string:
            id: "security-patch-bot-conduit-token"
            description: "Used to notify merge conflicts when applying security patches"
            secret: "${security_patch_bot_conduit_token}"
            scope: GLOBAL
jenkins:
  securityRealm:
    ldap:
      configurations:
        - server: "ldaps://ldap-ro.eqiad.wikimedia.org:636 ldaps://ldap-ro.codfw.wikimedia.org:636"
          rootDN: "dc=wikimedia,dc=org"
          userSearch: "cn={0}"
          groupSearchBase: "ou=groups"
          inhibitInferRootDN: false
      disableMailAddressResolver: false
      disableRolePrefixing: true
      groupIdStrategy: "caseInsensitive"
      userIdStrategy: "caseInsensitive"
  authorizationStrategy:
    projectMatrix:
      entries:
        - group:
            name: "releng"
            permissions:
            - "Overall/Administer"
        - user:
            name: "anonymous"
            permissions:
            - "Overall/Read"
        - user:
            name: "Brennen Bearnes"
            permissions:
            - "Overall/Administer"
        - user:
            name: "Giuseppe Lavagetto"
            permissions:
            - "Overall/Administer"
        - user:
            name: "Jaime Nuche"
            permissions:
            - "Overall/Administer"
        - user:
            name: "Jeena Huneidi"
            permissions:
            - "Overall/Administer"
        - user:
            name: "Jforrester"
            permissions:
            - "Overall/Administer"
        - user:
            name: "catrope"
            permissions:
            - "Overall/Administer"
        - user:
            name: "dduvall"
            permissions:
            - "Overall/Administer"
        - user:
            name: "hashar"
            permissions:
            - "Overall/Administer"
        - user:
            name: "legoktm"
            permissions:
            - "Overall/Administer"
        - user:
            name: "reedy"
            permissions:
            - "Overall/Administer"
        - user:
            name: "thcipriani"
            permissions:
            - "Overall/Administer"
        - user:
            name: "BryanDavis"
            permissions:
            - "Overall/Administer"
        - user:
            name: "Dzahn"
            permissions:
            - "Overall/Read"
        - user:
            name: "mstyles"
            permissions:
            - "Overall/Read"
        - user:
            name: "sbassett"
            permissions:
            - "Overall/Read"
  disabledAdministrativeMonitors:
    - "hudsonHomeIsFull"
    - "OldData"
    - "jenkins.security.QueueItemAuthenticatorMonitor"
    - "jenkins.security.ResourceDomainRecommendation"
    - "jenkins.security.s2m.MasterKillSwitchWarning"
    - "hudson.diagnosis.ReverseProxySetupMonitor"
  labelAtoms:
    - name: "built-in"
    - name: "${LOCAL_HOSTNAME}"
    - name: "blubber"
    - name: "dockerPublish"
    - name: "pipelinelib"
  markupFormatter:
    rawHtml:
      disableSyntaxHighlighting: false
  mode: EXCLUSIVE
  noUsageStatistics: true
  nodes:
    - permanent:
        name: "${LOCAL_HOSTNAME}"
        labelString: "pipelinelib blubber dockerPublish"
        numExecutors: 4
        launcher:
          ssh:
            credentialsId: "releases_jenkins_rsa"
            host: "localhost"
            javaPath: "/usr/bin/java"
            port: 22
            sshHostKeyVerificationStrategy:
              manuallyProvidedKeyVerificationStrategy:
                key: "${LOCALHOST_KEY}"
        remoteFS: "/srv/jenkins-agent"
        retentionStrategy: "always"
        nodeProperties:
        - envVars:
            env:
            - key: "http_proxy"
              value: "http://webproxy:8080"
            - key: "https_proxy"
              value: "http://webproxy:8080"
            - key: "no_proxy"
              value: "wikipedia.org,wikimedia.org,wikibooks.org,wikinews.org,wikiquote.org,wikisource.org,wikiversity.org,wikivoyage.org,wikidata.org,wikiworkshop.org,wikifunctions.org,wiktionary.org,mediawiki.org,wmfusercontent.org,w.wiki,wikimediacloud.org,wmnet,127.0.0.1,::1"

  numExecutors: 0
  proxy:
    name: "url-downloader.wikimedia.org"
    port: 8080
    noProxyHost: "*.wikimedia.org"
  quietPeriod: 0
  slaveAgentPort: -1
  systemMessage: "This is the Jenkins installation responsible for the weekly branch
    \ cut of MediaWiki and extensions for Wikimedia's production infrastructure. It
    \ is completely disconnected from the instance hosted at https://integration.wikimedia.org.
    \ <p>Jobs are defined using the Job DSL plugin. Definitions are in the
    \ <a href=\"https://gitlab.wikimedia.org/repos/releng/jenkins-deploy\">Jenkins deploy repo</a>. Please do not
    \ create/edit jobs manually.</p>"

security:
  scriptApproval:
    approvedScriptHashes:
      - "017d0db438428731cd600b6ebda805065433520f"
      - "0ca34f9d8a835d9308c48ced13a0735a46472dcf"
      - "2be37fb88d3c11081c07055c5c49ba5029ef359b"
      - "c7328090ce4dbea3ebfe66008b7c2481854dcc25"
      - "e12875d604dbf880e1e1593f62b81faa88ee696b"
    approvedSignatures:
      - "method groovy.lang.GroovyObject invokeMethod java.lang.String java.lang.Object"

unclassified:
  ansiColorBuildWrapper:
    globalColorMapName: "xterm"
  defaultFolderConfiguration:
    healthMetrics:
      - worstChildHealthMetric:
          recursive: true
  email-ext:
    defaultRecipients: "releng+jenkins-rel@wikimedia.org"
    allowedDomains: "@wikimedia.org , @lists.wikimedia.org"
    defaultContentType: "text/plain"
  globalLibraries:
    libraries:
      - defaultVersion: "master"
        name: "wikimedia-integration-pipelinelib"
        retriever:
          modernSCM:
            scm:
              git:
                id: "a35e38dc-2b9a-41cd-8fc0-0516f0671868"
                remote: "https://gerrit.wikimedia.org/r/integration/pipelinelib"
  location:
    adminAddress: "Jenkins Releases <releng@lists.wikimedia.org>"
    url: "https://releases-jenkins.wikimedia.org/"
  mavenModuleSet:
    localRepository: "perJob"
  prometheusConfiguration:
    countAbortedBuilds: false
    countFailedBuilds: false
    countNotBuiltBuilds: false
    countSuccessfulBuilds: true
    countUnstableBuilds: false
    defaultNamespace: "releases"
    fetchTestResults: false
    jobAttributeName: "job"
  buildDiscarders:
    configuredBuildDiscarders:
      - "jobBuildDiscarder"
      - simpleBuildDiscarder:
          discarder:
            logRotator:
              daysToKeepStr: "60"
tool:
  git:
    installations:
      - home: "git"
        name: "Default"
      - name: "jgit"
# This groovy code could alternatively go in the usual "init.groovy" script. I put it here to keep all config in a single
# location
groovy:
  - script: |
        import hudson.model.User
        import jenkins.security.ApiTokenProperty

        def ensureApiUserHasPredefinedToken() {
          User.get(System.getenv('API_USER')).getProperty(ApiTokenProperty.class).tokenStore.with { apiUserTokenStore ->
            if (!apiUserTokenStore.getTokenListSortedByName().find { it.name == 'token' }) {
              // Note the token is not an actual secret. We use the user/token to work around the problem described in
              // https://phabricator.wikimedia.org/T338950
              apiUserTokenStore.addFixedNewToken("token", '11006dc3b82f367a5a99c3ccd71dafe8c7')
            }
          }
        }

        ensureApiUserHasPredefinedToken()
