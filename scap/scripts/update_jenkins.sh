#!/usr/bin/env bash

set -eu -o pipefail

DEPLOYMENT_DIR=/srv/deployment/releng/jenkins-deploy
JENKINS_HOME=/var/lib/jenkins
HTTP_PROXY=
HTTP_PROXY_PORT=

### Setup

# When on a production host, use the available proxy to download plugins
if dnsdomainname | grep -q wmnet; then
  HTTP_PROXY=url-downloader.wikimedia.org
  HTTP_PROXY_PORT=8080
fi

# Create non-sensitive Jenkins env vars for the CasC configuration
{
  echo LOCALHOST_KEY="$(ssh-keyscan -t rsa localhost 2>/dev/null | sed 's/localhost //')"
  echo LOCAL_HOSTNAME="$(hostname --fqdn)"
  echo API_USER=jenkinsrelapi
} >/srv/deployment/releng/jenkins-deploy/jenkins_env

# Create CasC configuration file for defined jobs
"$(dirname "$0")"/generate_casc_jobs.sh "$DEPLOYMENT_DIR"

### Update

# First, reload any potential changes to the systemd unit config files
sudo systemctl daemon-reload
# Then install/update Jenkins
sudo apt-get install -y jenkins
# Finally install/update plugins (scap will restart the service at the end and the plugins will be picked up)
sudo -u jenkins \
	java -Dhttps.proxyHost="$HTTP_PROXY" -Dhttps.proxyPort="$HTTP_PROXY_PORT" \
  -jar $DEPLOYMENT_DIR/jenkins-plugin-manager-2.12.9.jar \
  --verbose \
  --war /usr/share/java/jenkins.war \
  -d $JENKINS_HOME/plugins \
  --clean-download-directory \
  --plugin-file "$(cat $DEPLOYMENT_DIR/plugins_file_location)"
