#!/usr/bin/env bash

set -eu -o pipefail

# Ensure consistent sorting of the jobs/*.groovy glob pattern match below and
# that _common.groovy comes first
export LC_COLLATE=C

(($# == 0)) && echo "Path to deployment dir needs to be specified" && exit 1
DEPLOYMENT_DIR=$1
CASC_DIR="$DEPLOYMENT_DIR"/conf/releasing/casc/

# shellcheck disable=SC2016
JOBS=$(sed -s 's/^/      /;$G' "$CASC_DIR"/jobs/*.groovy)
cat <<HERE >"$CASC_DIR"/jobs.yaml
jobs:
  - script: |
$JOBS
HERE
