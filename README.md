
# Scap deployment

Calls to "scap deploy" should be done with the "-f" flag to make sure the deployment always takes place, even if there
are no file changes (we still want the Jenkins package to be updated).

There's no default deployment defined. In order to deploy, one of the two environments (ci & releasing) needs to be
specified. Bundled script "deploy.sh" can be used to perform a full deploy.

## Deployment specifics

### Deploying to https://releases-jenkins.wikimedia.org/

You must be a member of the [`deployment-jenkins` group][0] to deploy the releases Jenkins.

Deployment installs the latest Jenkins from apt and sets all plugins to the versions in `conf/releasing/plugins.txt`
before restarting the Jenkins systemd service. This process is safe and should not cause any production alerts.

As long as you do not interfere with the scheduled run of long-running jobs and are aware of the consequences of the
apt install and plugin updates, then you may deploy this repo at any time.

Steps:

    ssh deployment.eqiad.wmnet
    cd /srv/deployment/releng/jenkins-deploy
    git pull --rebase
    ./deploy.sh

# Testing the deployment

We have a [development environment](https://gitlab.wikimedia.org/repos/releng/scap3-dev) that can be used to test
changes to the deployment. See the README there for more details.


[0]: <https://gerrit.wikimedia.org/r/plugins/gitiles/operations/puppet/+/44bbea9e10b4fe835f6c88f83b8c5c2b6a2cbbdd/modules/admin/data/data.yaml#335>
